FROM java:8
LABEL maintainer="cho750@gmail.com"
ARG JAR_FILE=target/TodoList-0.0.1-SNAPSHOT.jar
add ${JAR_FILE} todolist.jar
ENTRYPOINT ["java","-jar","/todolist.jar"]
